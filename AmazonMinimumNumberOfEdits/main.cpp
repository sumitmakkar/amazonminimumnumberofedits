//http://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

class Engine
{
    private:
        string str1;
        string str2;
        vector< vector<int> > matrixVector;
    
        void initializeMatrix()
        {
            int outerSize = (int)str1.size();
            int innerSize = (int)str2.size();
            for(int i = 0 ; i <= outerSize ; i++)
            {
                vector<int> innerVector;
                for(int j = 0 ; j <= innerSize ; j++)
                {
                    if(!i)
                    {
                        innerVector.push_back(j);
                    }
                    else if(!j)
                    {
                        innerVector.push_back(i);
                    }
                    else
                    {
                        innerVector.push_back(0);
                    }
                }
                matrixVector.push_back(innerVector);
            }
        }
    
        void displayMatrixVector()
        {
            int oSize = (int)str1.size();
            int iSize = (int)str2.size();
            for(int i = 0 ; i <= oSize ; i++)
            {
                for(int j = 0 ; j <= iSize ; j++)
                {
                    cout<<matrixVector[i][j]<<" ";
                }
                cout<<endl;
            }
        }
    
    public:
        Engine(string s1 , string s2)
        {
            str1 = s1;
            str2 = s2;
            initializeMatrix();
//            displayMatrixVector();
        }
    
        int getMinimumNumberOfEdits()
        {
            int outerSize = (int)str1.size();
            int innerSize = (int)str2.size();
            for(int i = 1 ; i <= outerSize ; i++)
            {
                for(int j = 1 ; j <= innerSize ; j++)
                {
                    if(str1[i-1] == str2[j-1])
                    {
                        matrixVector[i][j] = matrixVector[i-1][j-1];
                    }
                    else
                    {
                        int smallestValue  = min(matrixVector[i][j-1] , matrixVector[i-1][j]);
                        smallestValue      = min(smallestValue , matrixVector[i-1][j-1]);
                        matrixVector[i][j] = smallestValue+1;
                    }
                }
            }
            return matrixVector[outerSize][innerSize];
        }
};

int main(int argc, const char * argv[])
{
    string str1 = "CART";
    string str2 = "MARCH";
    Engine e    = Engine(str1 , str2);
    cout<<e.getMinimumNumberOfEdits()<<endl;
    return 0;
}
